<?php

namespace App\DataFixtures;

use App\Entity\Advertisement;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use App\Entity\User;
use DateTime;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $hasher;

    public function __construct(UserPasswordHasherInterface $hasher)
    {
        $this->hasher = $hasher;
    }

    public function load(ObjectManager $manager): void
    {
            $user = new User();
            $user->setName('user1');
            $user->setLastName('lastName1');
            $user->setEmail('hugo.lyon@gmail.com');
            $user->setTel('0678905486');
            $user->setRoles(['ADMIN']);
            $password = $this->hasher->hashPassword($user, 'pass_1234');
            $user->setPassword($password);
            $manager->persist($user);

            $user2 = new User();
            $user2->setName('user2');
            $user2->setLastName('lastName2');
            $user2->setEmail('Maurice.lyon@gmail.com');
            $user2->setTel('0678905468');
            $user2->setRoles(['USER']);
            $password2 = $this->hasher->hashPassword($user2, 'pass_1234');
            $user2->setPassword($password2);
            $manager->persist($user2);

            $post = new Advertisement();
            $post->setDate((new DateTime()));
            $post->setPerson($user2);
            $post->setPostalCode('69100');
            $post->setPicture('avatar.png');
            $post->setText('bonjour, je vend ma voiture sortie neuve');
            $post->setTitre('Voiture');
            $post->setTel('0678905468');
            $manager->persist($post);

            $post2 = new Advertisement();
            $post2->setDate((new DateTime()));
            $post2->setPerson($user);
            $post2->setPostalCode('69100');
            $post2->setPicture('avatar.png');
            $post2->setText('bonjour, je vend un vélo');
            $post2->setTitre('Vélo');
            $post2->setTel('0678905468');
            $manager->persist($post2);

        $manager->flush();
    }
}
