<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends AbstractController
{
    /**
    * @Route("/register", name="app_register")
    */
    public function register(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User;
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setRoles(['ADMIN']);
            $hashedPassword = $encoder->encodePassword($user, $form->get('plainPassword')->getData());
            $user->setPassword($hashedPassword);
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('app_profile_user');
        }

        return $this->render('user/formAccount.html.twig', [
            'UserForm' => $form->createView()
        ]);
    }

    /** 
    * @Route("/user/{id}", name="app_profile_user")
    */ 
    public function index(User $user): Response
    {
        return $this->render('user/index.html.twig', [
            'profile_user' => $user,
        ]);
    }

    /**
     * @Route("/edit/profile/{id}", name="app_edit_profile")
     */
    public function edit($id, Request $request)
    {
        $entityUser = $this->getDoctrine()->getManager();
        $user = $entityUser->getRepository(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityUser->persist($user);
            $entityUser->flush();
            return $this->redirectToRoute('app_profile_user', [
                'id' => $user->getId()
            ]);
        }
        return $this->render('user/formAccount.html.twig', [
            'UserForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/user/{id}", name="app_delete_profile_user")
     */
    public function deleteUser(User $user)
    {
        $user = $this->getUser();
        $delete = $this->getDoctrine()->getManager();
        $delete->remove($user);
        $delete->flush();
        return $this->redirectToRoute('app_login');
    }
}
