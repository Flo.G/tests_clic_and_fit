<?php

namespace App\Controller;

use App\Entity\Advertisement;
use App\Form\AdvertisementType;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

class AdvertisementController extends AbstractController
{
    /**
     * @Route("/advertisement", name="app_advertisement")
     */
    public function findAll(Request $request, ManagerRegistry $doctrine, PaginatorInterface $paginator): Response
    {
        $advertisement = $doctrine->getRepository(Advertisement::class)->findAll();
        $announces = $paginator->paginate(
            $advertisement,
            $request->query->getInt('page', 1), 
            1 
        );

        return $this->render('advertisement/index.html.twig', [
            'announces' => $announces,
        ]);
    }

    /**
     * @Route("/add/announces", name="app_add_announce")
     */
    public function form(Request $request,  EntityManagerInterface $manager, FileUploader $FileUploader)
    {
        $announce = new Advertisement;
        $form = $this->createForm(AdvertisementType::class, $announce);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //$ImgPath = $FileUploader->upload($announce->fileImg);
            $announce->setPerson($this->getUser());
            //$announce->setPicture($ImgPath);
            $manager->persist($announce);
            $manager->flush();
        }

        return $this->render('advertisement/form.html.twig', [
            'AdvertisementForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit_announce/{id}", name="app_edit_announce")
     */
    public function modify($id, Request $request, FileUploader $FileUploader)
    {
        
        $entityAdver = $this->getDoctrine()->getManager();
        $announceForm = $entityAdver->getRepository(Advertisement::class)->find($id);
        
        if (!$announceForm) {
            throw $this->createNotFoundException(
                'No product found for id ' . $id
            );
        }
        $form = $this->createForm(AdvertisementType::class, $announceForm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //$ImgPath = $FileUploader->upload($announceForm->fileImg);
            $announceForm->setPerson($this->getUser());
            //$announceForm->setPicture($ImgPath);
            $entityAdver->persist($announceForm);
            $entityAdver->flush();
            return $this->redirectToRoute('app_advertisement');
        }
        return $this->render('advertisement/form.html.twig', [
            'AdvertisementForm' => $form->createView()
        ]);
    }
}
